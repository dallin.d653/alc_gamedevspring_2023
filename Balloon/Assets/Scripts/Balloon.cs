using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balloon : MonoBehaviour
{
    public int clickToPop = 3;

    public float scaleToIncrease = 0.10f;

    public int scoreToGive = 100;

    private ScoreManager scoreManager;

    void Start()
    {
        scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
    }

    void OnMouseDown()
    {
        clickToPop -= 1;

        transform.localScale += Vector3.one * scaleToIncrease;

        if(clickToPop == 0) 
        {
            Destroy(gameObject);
            scoreManager.IncreaseScoreText(scoreToGive);         
        }
    }
}
