using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatUP : MonoBehaviour
{
    public float moveSpeed = 1.0f;
    public float upperBound = 25.0f;

    private Balloon balloon;
    private ScoreManager scoreManager;

    // Start is called before the first frame update
    void Start()
    {
        balloon = GetComponent<Balloon>();
        scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.up * moveSpeed * Time.deltaTime);

        if(transform.position.y > upperBound)
        { 
            Destroy(gameObject); // Pop balloon
            scoreManager.DecreaseScoreText(balloon.scoreToGive);
        }
    }
}
