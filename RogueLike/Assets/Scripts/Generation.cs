using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generation : MonoBehaviour
{
    [Header("Map Properties")]
    public int mapWidth = 5;
    public int mapHeight = 5;
    public int roomsToGenerate = 9;
    public int seed;
    public float roomSize = 12f;
    public float tileSize = 0.16f;

    private int roomCount;
    private bool roomsInstantiated;

    // Store the origin of the first room to generate procedural dungeon
    private Vector2 firstRoomPos;

    // A 2D Boolean Array to map out the level
    private bool[,] map;

    // Room Prefab to instantiate
    public GameObject roomPrefab;

    private List<Room> roomObjects = new List<Room>();

    //Creating a Singleton
    public static Generation instance;

    void Awake()
    {
        instance = this; 
    }

    // Start is called before the first frame update
    void Start()
    {
        // Random seed assigned to random number generator
        Random.InitState(seed);
        Generate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Generate()
    {
        // Create a new map of the specified size
        map = new bool[mapWidth, mapHeight];
        // Check to see if we can place a room in the center of the map
        CheckRoom(3, 3, 0, Vector2.zero, true);
        InstantiateRooms();
        // Find the player in the scene, and position them inside the first room
        FindObjectOfType<PlayerController2D>().transform.position = firstRoomPos * roomSize * tileSize;
    }

    void CheckRoom(int x, int y, int remaining, Vector2 generalDirection, bool firstRoom = false) 
    {
        // If we have generated all of the rooms that we need, stop the function.
        if(roomCount >= roomsToGenerate)
            return;
        // If this is outside the bounds of the actual map, stop the funtion.
        if(x < 0 || x > mapWidth -1 || y < 0 || y > mapHeight - 1)
            return;
        // If this is not the first room, and there is no more room to check, stop the functoin.
        if(!firstRoom && remaining <= 0)
            return;
        //If the given map tile is already occupied, stop the function.
        if(map[x,y])
            return;
        // If this is the first room, store the room position.
        if(firstRoom)
            firstRoomPos = new Vector2(x,y);
        // Add one to roomCount and set the map tile to be true;
        roomCount++;
        map[x,y] = true;

        // North will be true if the random value is greater than 0.2f (if generalDirection == up) or greater than 0.8f (when generalDirection != up)
        bool north = Random.value > (generalDirection == Vector2.up ? 0.2f : 0.8f);
        bool south = Random.value > (generalDirection == Vector2.down ? 0.2f : 0.8f);
        bool east = Random.value > (generalDirection == Vector2.right ? 0.2f : 0.8f);
        bool west = Random.value > (generalDirection == Vector2.left ? 0.2f : 0.8f);

        int maxRemaining = roomsToGenerate / 4;

        //if north is true, make a room one tile above the current
        if (north || firstRoom)
            CheckRoom(x,y + 1, firstRoom ? maxRemaining : remaining -1, firstRoom? Vector2.up : generalDirection);
        if (south || firstRoom)
            CheckRoom(x,y - 1, firstRoom ? maxRemaining : remaining -1, firstRoom? Vector2.down : generalDirection);
        if (east || firstRoom)
            CheckRoom(x + 1,y, firstRoom ? maxRemaining : remaining -1, firstRoom? Vector2.right : generalDirection);
        if (west || firstRoom)
            CheckRoom(x - 1,y, firstRoom ? maxRemaining : remaining -1, firstRoom? Vector2.left : generalDirection);
    }

    void InstantiateRooms()
    {
        if(roomsInstantiated)
            return;

        roomsInstantiated = true;

        for(int x = 0; x < mapHeight; x++)
        {
            for(int y = 0; y < mapHeight; y++)
            {
                if(map[x,y] == false)
                    continue;

                //instance a new room prefab
                GameObject roomObj = Instantiate(roomPrefab, new Vector3(x,y,0) * roomSize * tileSize, Quaternion.identity);

                // get reference to the room script of the new room object
                Room room = roomObj.GetComponent<Room>();

                //If we're within the boundry of the map, and if there is a room above us
                if(y < mapHeight - 1 && map[x,y + 1] == true)
                {
                    //enable the north door and disable the north wall
                    room.northDoor.gameObject.SetActive(true);
                    room.northWall.gameObject.SetActive(false);
                }

                //If we're within the boundry of the map, and if there is a room below us
                if(y > 0 && map[x,y - 1] == true)
                {
                    //enable the south door and disable the south wall
                    room.southDoor.gameObject.SetActive(true);
                    room.southWall.gameObject.SetActive(false);
                }

                //If we're within the boundry of the map, and if there is a room right of us
                if(x < mapWidth - 1 && map[x + 1,y] == true)
                {
                    //enable the east door and disable the east wall
                    room.eastDoor.gameObject.SetActive(true);
                    room.eastWall.gameObject.SetActive(false);
                }

                //If we're within the boundry of the map, and if there is a room left us
                if(x > 0 && map[x - 1,y] == true)
                {
                    //enable the west door and disable the west wall
                    room.westDoor.gameObject.SetActive(true);
                    room.westWall.gameObject.SetActive(false);
                }

                // If this is not the first room, call GenerateInterior
                if(firstRoomPos != new Vector2(x,y))
                    room.GenerateInterior();

                // Add the room to the roomObject
                roomObjects.Add(room);
            }
        }

        //After looping through every element inside the map array, call CalculateKeyandExit
        CalculateKeyandExit();

    }

    //Places the key and exit in the level
    void CalculateKeyandExit()
    {
        
    }
}
