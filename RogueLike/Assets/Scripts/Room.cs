using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{

    // References for door objects
    [Header("Door Objects")]
    public Transform northDoor;
    public Transform southDoor;
    public Transform eastDoor;
    public Transform westDoor;
    
    // References for wall objects
    [Header("Wall Objects")]
    public Transform northWall;
    public Transform southWall;
    public Transform eastWall;
    public Transform westWall;
    
    // How many tiles are there in the room
    [Header("Room Size")]
    public int insideWidth;
    public int insideHeight;

    // Objects to instantiate
    [Header("Room Prefabs")]
    public GameObject enemyPrefab;
    public GameObject coinPrefab;
    public GameObject healthPrefab;
    public GameObject keyPrefab;
    public GameObject exitDoorPrefab;

    // List of positions to aviod layering objects while instantiating
    private List<Vector3> usedPositions = new List<Vector3>();

    public void GenerateInterior() 
    {
        // Generate coins, enemies, health, etc.

    }

    public void SpawnPrefab(GameObject prefab, int min = 0, int max = 0) 
    {
        // Spawn
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
