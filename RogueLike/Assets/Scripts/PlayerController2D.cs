using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerController2D : MonoBehaviour
{
    [Header("Attributes")]
    public int hp;
    public int maxHP;
    public float speed;
    public float attackDistance;
    public int attackDamage;
    public int coins;
    public float delay;

    [Header("References")]
    public SpriteRenderer sr;
    public LayerMask moveLayerMask;
    public float tileSize;

    // Privates
    private bool hasKey;
    
    void Start()
    {
        attackDistance = attackDistance * 0.16f;
    }

    void Move(Vector2 dir)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, tileSize, moveLayerMask);

        if(hit.collider == null)
        {
            transform.position += new Vector3(dir.x * tileSize, dir.y * tileSize,0);

            EnemyManager.instance.OnPlayerMove();
        }
    }

    public void OnMoveUp(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            Move(Vector2.up);
    }

    public void OnMoveDown(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            Move(Vector2.down);
    }

    public void OnMoveLeft(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            Move(Vector2.left);
    }

    public void OnMoveRight(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            Move(Vector2.right);
    }

    public void OnAttackUp(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            Move(Vector2.up);
    }

    public void OnAttackDown(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            Move(Vector2.down);
    }

    public void OnAttackLeft(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            Move(Vector2.left);
    }

    public void OnAttackRight(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            Move(Vector2.right);
    }
    
    public void TakeDamage(int damageToTake)
    {
        hp -= damageToTake;
        StartCoroutine(DamageFlash());

        if(hp <=0)
            SceneManager.LoadScene(0);
    }

    IEnumerator DamageFlash()
    {
        // get a refernce to the default sprite color
        Color defaultColor = sr.color;
        // set the color to white
        sr.color = Color.red;
        // wait for a period of time before changing color
        yield return new WaitForSeconds(delay);
        // set the color back to default
        sr.color = defaultColor;
    }

    void TryAttack(Vector2 dir)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, attackDistance);

        if (hit.collider != null)
            hit.transform.GetComponent<Enemy>().TakeDamage(attackDamage);
    }

    
    
    
    
    // private float hInput;
    // private float vInput;
    // public float moveSpeed;
    // public float turnSpeed;
    // Rigidbody2D rb;

    // // Start is called before the first frame update
    // void Start()
    // {
    //     rb = GetComponent<Rigidbody2D>();
    // }

    // // Update is called once per frame
    // void Update()
    // {
    //     hInput = Input.GetAxis("Horizontal");
    //     vInput = Input.GetAxis("Vertical");

    //     rb.velocity = new Vector2(hInput * moveSpeed, vInput * moveSpeed);
    // }
}
    

