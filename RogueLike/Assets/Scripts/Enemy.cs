using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [Header("Attributes")]
    public int health;
    public int AttackDamage;
    public float delay;
    public float  attackChance = 0.5f;

    [Header("References")]
    public GameObject deathDropPrefab;
    public SpriteRenderer sr;
    public PlayerController2D player;
    public LayerMask moveLayerMask;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController2D>();
    }

    public void Move()
    {
        if(Random.value < 0.5f)
            return;
        
        Vector3 dir = Vector3.zero;
        bool canMove = false;

        // before moving the enemy, get a random direction to move to.
        while(!canMove)
        {
            dir = GetRandomDir();
            // cast a ray into dir
            RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, 0.16f, moveLayerMask);

            // if the ray hasn't detected any obstacle.
            if(hit.collider == null)
                canMove = true;

            // end of while loop
        }

        transform.position += dir * 0.16f;
    }

    public Vector3 GetRandomDir()
    {
        int ran = Random.Range(0, 4);

        switch(ran)
        {
            case 0: return Vector3.up;
            case 1: return Vector3.down;
            case 2: return Vector3.left;
            case 3: return Vector3.right;
        }
        return Vector3.zero;
    }

    public void TakeDamage(int damage)
    {
        health -= damage;

        if(health <= 0)
        { 
            if(deathDropPrefab != null) 
                Instantiate(deathDropPrefab, transform.position, transform.rotation);

            Destroy(gameObject);
        }

        StartCoroutine(DamageFlash());

        if(Random.value > attackChance)
        player.TakeDamage(damage);
    }

    IEnumerator DamageFlash()
    {
        // get a refernce to the default sprite color
        Color defaultColor = sr.color;
        // set the color to white
        sr.color = Color.red;
        // wait for a period of time before changing color
        yield return new WaitForSeconds(delay);
        // set the color back to default
        sr.color = defaultColor;
    }
}
