using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{

    [Header("Movement")]
    public float hInput;
    public float vInput;
    public float moveSpeed;
    public float rotationSpeed;
    public KeyCode jumpKey = KeyCode.Space;
    public float jumpForce;
    Rigidbody rb;
    public float playerHeight;
    public float groundDrag;
    bool grounded;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }    

    // Update is called once per frame
    void Update()
    {
        grounded = Physics.Raycast(transform.position, Vector3.down, playerHeight * 0.5f + 0.2f);
        hInput = Input.GetAxis("Horizontal"); //Left & Right
        vInput = Input.GetAxis("Vertical"); //Forward & Back

        if(grounded) rb.drag = groundDrag;
        else rb.drag = 0;

        transform.Rotate(Vector3.up * hInput * rotationSpeed * Time.deltaTime); //Move player in real time
        rb.AddForce(transform.forward * vInput * moveSpeed, ForceMode.Force); //Move player in real time
        Jump();
    }

    void Jump()
    {
        if(Input.GetKeyDown(jumpKey))
        {
            rb.AddForce(transform.up * jumpForce, ForceMode.Impulse);
        }
    }
}
