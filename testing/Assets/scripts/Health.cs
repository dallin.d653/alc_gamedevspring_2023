using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : PickUp
{
    // Start is called before the first frame update
    void Start()
    {
        name = "Health Pickup";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player")) Destroy(gameObject);
    }
}
