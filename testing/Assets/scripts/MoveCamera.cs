using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    public Transform Pos;

    // Update is called once per frame
    void Update()
    {
        transform.position = Pos.position;
    }
}
