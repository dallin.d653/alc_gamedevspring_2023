using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public Transform[] SpawnPoints;

    public KeyCode[] Keys;

    // Update is called once per frame
    void Update()
    {
        
        
    }

    void Teleport(int num)
    {
        if(Input.GetKeyDown(Keys[num])) transform.position = SpawnPoints[num].transform.position;
    }
}
